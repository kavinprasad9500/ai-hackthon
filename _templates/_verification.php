<div class="row">
    <div class="col-sm-02">
        <nav class="open">
            <div class="logo">
                <i class="bx bx-menu menu-icon"></i>
                <span class="logo-name">Online Exam</span>
            </div>
            <div class="sidebar">
                <div class="logo">
                    <i class="bx bx-menu menu-icon"></i>
                    <span class="logo-name">Online Exam</span>
                </div>

                <div class="sidebar-content">
                    <ul class="lists" style="padding-left: 0px;">
                        <li class="list" style="margin-right: 10%;">
                            <a href="#" class="">
                                <img src="./assests/images/profile.png" class=" rounded-circle img-thumbnail" alt="">
                            </a>
                        </li>
                        <li class="list">
                            <a href="#" class="nav-link">
                                <i class="fa-regular fa-user icon"></i>
                                <span class="link">Profile</span>
                            </a>
                        </li>
                        <li class="list">
                            <a href="#" class="nav-link">
                                <span class="link">Name : Kavin Prasad J</span>
                            </a>
                        </li>
                        <li class="list">
                            <a href="#" class="nav-link">
                                <span class="link">Regno : 20104055</span>
                            </a>
                        </li>
                        <li class="list">
                            <a href="#" class="nav-link">
                                <span class="link">Class : III CSE A</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="col-sm-10">

        <div class="container" style="margin-left: 15%;">
            <center>

                <div class="row justify-content-center">
                    <div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5 text-center p-0 mt-1 ">
                        <div class="card mt-2" style="background-color: transparent;">
                            <h2 id="heading">Checking Requirments</h2>
                            <form id="msform">
                                <ul id="progressbar">
                                    <li class="active" id="requirments"><strong>Requirments</strong></li>
                                    <li class="active" id="verification"><strong>Verification</strong></li>
                                    <li id="finish"><strong>Finish</strong></li>
                                </ul>
                                <br>
                            </form>
                        </div>
                    </div>
                </div>



                <div class="card mb-3" style="width: 50%;height:22rem;">
                    <div class="card-body">

                        <?php
                        $command = escapeshellcmd('/home/kavinprasad/htdocs/ai-hackthon/Face recognition using OpenCV/3.face_recognition.py');
                        $output = shell_exec($command);
                        echo $output;
                        ?>
                    </div>
                </div>



                <h4>
                    <div class="spinner-border text-success" role="status" style="margin-right: 10px;">
                        <span class="visually-hidden">Loading...</span>
                    </div>Verifing Face Recognition....
                </h4>
                <Button href="#" class="btn btn-success " disabled>Start</Button>

            </center>

        </div>
    </div>
</div>