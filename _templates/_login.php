    <!-- Section: Login Block -->


    <section class="background-radial-gradient overflow-hidden ">
        <div class="container px-4 py-5 px-md-5 text-center text-lg-start vh-100 ">
            <div class="row gx-lg-5 align-items-center mb-5 mt-5 ">
                <div class="col-lg-6 mb-5 mb-lg-0" style="z-index: 10">
                    <h1 class="my-5 display-5 fw-bold ls-tight" style="color: hsl(218, 81%, 95%)">
                        Welcome Back !<br />
                        <span style="color: hsl(218, 81%, 75%)">for your exams</span>
                    </h1>
                    <p class="mb-4 opacity-70" style="color: hsl(218, 81%, 85%)">
                        Defining the future of LEARNING. Fuelled by Technology, Powered for Tomorrow.
                    </p>
                </div>

                <div class="col-lg-6 mb-5 mb-lg-0 position-relative mt-5">
                    <div id="radius-shape-1" class="position-absolute rounded-circle shadow-5-strong"></div>
                    <div id="radius-shape-2" class="position-absolute shadow-5-strong"></div>

                    <div class="card bg-glass" style="border-radius: 40px;">
                        <div class="card-body px-4 py-5 px-md-5 text-center">
                            <h2 class="fw-bold mb-5">Login to continue</h2>
                            <?php
                            if (isset($_POST['user']) and isset($_POST['password'])) {
                                $username = $_POST['user'];
                                $password = $_POST['password'];
                                if ($username == "kavin_prasad" && $password == "Kavinprasad333@") {
                                    ?>
                                    <script>
                                        window.location.href = "./permission.php"
                                    </script>
                                <?php
                                } else {
                                    ?>
                                    <div class="alert alert-danger" role="alert">
                                        Enter valid Credentials!
                                        Check your username or password
                                    </div>
                            <?php
                                }
                            }

                            ?>
                            <form action="login.php" method="post">



                                <!-- Email input -->
                                <div class="form-floating mb-3">
                                    <input type="text" name="user" class="form-control" id="floatingInput" placeholder=" " required>
                                    <label for="floatingInput">Username</label>
                                </div>

                                <!-- Password input -->

                                <div class="form-floating mb-3">
                                    <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
                                    <label for="floatingPassword">Password</label>
                                </div>
                                <div class="mb-4">
                                    <input type="checkbox" class="form-check-input " id="remembercheck">
                                    <label class="form-check-label" for="exampleCheck1"> Remember me </label>

                                </div>


                                <!-- Submit button -->
                                <div class="d-grid gap-2 col-6 mx-auto">
                                    <button class="btn btn-primary" type="submit" id="login" >Login</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>


        </div>

    </section>
