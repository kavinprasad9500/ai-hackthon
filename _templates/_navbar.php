<nav class="navbar navbar-expand-lg bg-light">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="https://getbootstrap.com/docs/5.2/assets/brand/bootstrap-logo.svg" alt="Logo" width="30" height="24" class="d-inline-block align-text-top">
        Online Exam
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">

        </ul>
        <form class="d-flex" role="search">
          <!-- <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search"> -->
          <h5 class="pt-2">Kavin Prasad J</h5>
          <!-- <button class="btn btn-outline-success" type="submit">Search</button> -->
          <ul class="header-nav me-4">
            <li class="nav-item dropdown d-flex align-items-center blur-wrapper"><a class="nav-link py-0" data-coreui-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <div class="avatar avatar-md">
                  <style>
                    .avatar-img {
                      width: 40px;
                      height: 40px;
                      border-radius: 50%;
                    }
                  </style>
                  <img class="avatar-img" src="https://git.selfmade.ninja/uploads/-/system/user/avatar/2924/avatar.png" alt="kavinprasad333@gmail.com">
                  <span id="vpn_avatar_status" class="avatar-status bg-success"></span>

                </div>
              </a>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </nav>