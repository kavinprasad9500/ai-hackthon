<div class="container">
    <center>

        <div class="row justify-content-center">
            <div class="col-11 col-sm-9 col-md-7 col-lg-6 col-xl-5 text-center p-0 mt-3 ">
                <div class="card mt-3 mb-2">
                    <h2 id="heading">Checking Requirments</h2>
                    <form id="msform">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active" id="requirments"><strong>Requirments</strong></li>
                            <li id="verification"><strong>Verification</strong></li>
                            <li id="finish"><strong>Finish</strong></li>
                        </ul>
                        <br>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 card text-center mb-5 ">
                <div class="card-body">
                    <h3 class="card-title">Checking Internet Stability</h3>
                    <center>
                        <lottie-player src="https://lottie.host/7d5d66f3-ad69-4f23-ba0a-acdfae396541/je5gZKpiqB.json" background="transparent" speed="1" style="width: 200px; height: 200px;" autoplay loop></lottie-player>
                    </center>
                </div>
            </div>
            <div class="col-sm-4 card text-center mb-5 ">
                <div class="card-body">
                    <h3 class="card-title">Checking Camera Quality</h3>
                    <center>
                        <lottie-player src="https://assets1.lottiefiles.com/packages/lf20_mvof7nsb.json" background="transparent" speed="1" style="width: 200px; height: 200px;" autoplay loop></lottie-player>
                    </center>
                </div>
            </div>
            <div class="col-sm-4 card text-center mb-5">
                <div class="card-body">
                    <h3 class="card-title">Checking Microphone</h3>
                    <center>
                        <lottie-player src="https://assets7.lottiefiles.com/packages/lf20_azafausa.json" background="transparent" speed="1" style="width: 200px; height: 200px;" autoplay loop></lottie-player>
                    </center>
                </div>
            </div>
        </div>


        <h4>
            <div class="spinner-border text-success" role="status" style="margin-right: 10px;">
                <span class="visually-hidden">Loading...</span>
            </div>Please Wait...
        </h4>
        <Button href="#" class="btn btn-success mb-5" disabled>Success</Button>

    </center>


</div>