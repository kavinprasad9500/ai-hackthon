<div class="container mt-5">

    <h5 style="width: 90%;">1. Write a Python Program <br>
        Given an unsorted array A of size N that contains only non-negative integers, find a continuous sub-array which adds to a given number S.

        In case of multiple subarrays, return the subarray which comes first on moving from left to right.
    </h5>


    <textarea style="width: 90%; height: 350px; border: 2px solid #ddd;background-color:#F8F8F8;" class="m-3"></textarea>

    <ul class="nav justify-content-end border-bottom pb-3 ">
        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted"><button type="button" class="btn btn-secondary">Next </button></a></li>
        <li class="nav-item"><a href="#" class="nav-link px-2 text-muted"><button type="button" class="btn btn-primary">Submit</button></a></li>
    </ul>
</div>