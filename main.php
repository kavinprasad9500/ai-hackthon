<!DOCTYPE html>
<html lang="en">


<?php

include 'libs/load.php';


load_template('_head');
?>
<link href="./assests/css/style.css" rel="stylesheet" />

<body>

    <?php
    load_template('_navbar');
load_template('_floatCamera');
load_template('_quiz');
load_template('_footer');
load_template('_scripts');
?>
    <script src="./assests/js/main.js"></script>

</body>

</html>